# Online User Component

## Pre-req

- Node 10
- Yarn
- NPM 6.9.0

or run `nvm use` if nvm part of your dev workflow

## Install

`npm i` or `yarn install`

## Run

`npm run build` or `yarn build`

## Tests

`npm run test` or `yarn test`