import React from 'react';
import { render } from 'react-dom';

import OnlineUser from '../components/OnlineUser/OnlineUser';

(() => {
    const onlineUserContainer = document.getElementById('online-user-container');
    if (onlineUserContainer != null) {
        render(
            <OnlineUser />,
            onlineUserContainer
        )
    }
})()