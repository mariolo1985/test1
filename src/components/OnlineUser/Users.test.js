import React from 'react';
import { render } from 'enzyme';
import { Users } from './Users';

describe('Footer component', () => {
    let usersRenderCopy;
    beforeEach(() => {
        const users = [
            {
                av: 'av.jpg',
                name: 'Bob Smith',
                online: false
            },
            {
                av: 'av.jpg',
                name: 'Jane Smith',
                online: true
            },
            {
                av: 'av.jpg',
                name: 'John Doe',
                online: true
            },
            {
                av: 'av.jpg',
                name: 'Lewis Hamilton',
                online: false
            },
            {
                av: 'av.jpg',
                name: 'Larry O Brien',
                online: true
            }
        ]

        usersRenderCopy = render(
            <Users Users={users} />
        );

    });

    describe('Initial Render', () => {
        it('Renders correctly', () => {
            expect(usersRenderCopy.find(Users)).toMatchSnapshot();
        });
    });
});
