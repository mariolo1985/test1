import React from 'react';

export const Users = ({
  Users
}) => (
    Users.map((user) => {
      const {
        av,
        name,
        online
      } = user;

      return (
        <div className='user' key={name}>
          <div className='user-av'>
            <img src={`images/${av}`} className='user-av--img' />
          </div>
          <div className='user-name'>{name}</div>
          <div className='user-status'>
            <div className={`${online ? 'user-online' : 'user-offline'}`}></div>
          </div>
        </div>
      );
    })
  )