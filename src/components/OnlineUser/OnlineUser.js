import React, { Component } from 'react';
import { Users } from './Users';

class OnlineUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            isExpanded: false,
            isLoaded: false
        }
    }

    componentDidMount = () => {
        // fetch user
        fetch('/data/users.json', {
            method: 'GET'
        })
            .then((response) => {
                return response.json();
            })
            .then((userData) => {
                setTimeout(() => {
                    this.setState({
                        data: userData.users,
                        isExpanded: true,
                        isLoaded: true
                    })
                }, 2000);
            })
            .catch(err => console.log(err));
    }

    showHideUsers = () => {
        const { isExpanded } = this.state;
        this.setState({
            isExpanded: !isExpanded
        })
    }

    render() {
        const {
            data,
            isExpanded,
            isLoaded
        } = this.state;

        return (
            <div className={`online-users${isLoaded && ' online-users--loaded'}`}>
                <span>
                    {
                        isLoaded ?
                            'Online Users' :
                            'Fetching users...'
                    }
                </span>
                <button type='button' className={`btn${isLoaded ? '' : 'btn-disabled'}`} onClick={() => this.showHideUsers()}>
                    {
                        isExpanded ? 'Close' : 'Open'
                    }
                </button>
                <div className={`users${isExpanded ? ' users-show' : ' users-hide'}`} >
                    {
                        data.length > 0 &&
                        <Users Users={data} />
                    }
                </div>
            </div>
        );
    }
}

export default OnlineUser;