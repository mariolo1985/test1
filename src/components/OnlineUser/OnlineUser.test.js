import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { configure, render, shallow } from 'enzyme';
import OnlineUser from './OnlineUser';

const fetch = () => {
  return Promise.resolve({
    json: () =>
      Promise.resolve({
        "users": [
          {
            "av": "av.jpg",
            "name": "Bob Smith",
            "online": false
          },
          {
            "av": "av.jpg",
            "name": "Jane Smith",
            "online": true
          },
          {
            "av": "av.jpg",
            "name": "John Doe",
            "online": true
          },
          {
            "av": "av.jpg",
            "name": "Lewis Hamilton",
            "online": false
          },
          {
            "av": "av.jpg",
            "name": "Larry O Brien",
            "online": true
          }
        ]
      })
  })
};

global.fetch = fetch;
configure({ adapter: new Adapter() });

describe('Footer component', () => {
  let onlineuserRenderCopy;
  beforeEach(() => {
    onlineuserRenderCopy = render(
      <OnlineUser />
    );
  });

  describe('Initial Render', () => {
    it('Renders correctly', () => {
      expect(onlineuserRenderCopy.find(OnlineUser)).toMatchSnapshot();
    });
  });

  describe('Fetching users', () => {
    it('Fetches successfully', () => {
      const fetchSpy = jest.spyOn(window, 'fetch');
      const onlineUserInstance = shallow(<OnlineUser />);

      expect(fetchSpy).toBeCalled();
      expect(fetchSpy()
        .then((response) => {
          return response.json();
        })
        .then((userData) => {
          expect(userData.users).toHaveLength(5);
        })
      );
    })
  })
});
