const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const entries = [
  path.join(__dirname, 'src/js/index.js'),
  path.join(__dirname, 'src/css/master.css')
];

const devPlugins = [
  new webpack.HotModuleReplacementPlugin(),
  new webpack.LoaderOptionsPlugin({
    minimize: true
  }),
  new CopyWebpackPlugin([
    { from: 'src/*.html', to: path.join(__dirname, 'dist'), flatten: true },
    { from: 'src/images', to: path.join(__dirname, 'dist/images'), flatten: true },
    { from: 'src/data', to: path.join(__dirname, 'dist/data'), flatten: true }
  ]),
  new MiniCssExtractPlugin({
    filename: 'css/master.min.css'
  })
];

const moduleRules = [
  {
    test: /\.js$/,
    use: {
      loader: 'babel-loader'
    }
  },
  {
    test: /\.css$/,
    use: [
      MiniCssExtractPlugin.loader,
      'css-loader'
    ]
  }

]

module.exports = {
  name: 'Bundling dev',
  mode: 'development',
  entry: entries,
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'js/index.min.js'
  },
  devServer: {
    contentBase: './dist',
    hot: true,
    open: true
  },
  module: {
    rules: moduleRules
  },
  plugins: devPlugins
};
